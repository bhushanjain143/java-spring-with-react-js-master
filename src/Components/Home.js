import React,{useEffect} from "react";
import { Jumbotron} from "reactstrap";
import {
    Button,
    Container,
} from "reactstrap";


const Home = () => {
    useEffect(() => {
        document.title= "Home";
    },[]) // [] <- so it run only single time 

    return (

        <div>
            <Jumbotron className="text-center">
                <h1>Learn React with Spring Boot</h1>
                <h5>See me on</h5><h5> <a href="http://www.linkedin.com/in/yogiraj-patil-5040571a2">LinkedIn</a></h5>
                <a href="http://gitlab.com/yogiraj80555"><h6>Git Repo</h6></a>

            </Jumbotron>

        </div>
    )
}



export default Home;