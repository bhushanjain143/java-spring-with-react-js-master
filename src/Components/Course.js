import axios from "axios";
import React from "react"
import { Link } from "react-router-dom";
import { toast } from "react-toastify";

import {
    Card,
    CardBody,
    CardTitle,
    CardSubtitle,
    CardText,
    CardFooter,
    Button,
    Container,
} from "reactstrap";
import base_url from "../API/Apis";



const Course = ({ course,update }) =>{


const deleteCourse = (id) => {
    console.log(id);
    axios.delete(`${base_url}/course/`+id).then(
        (response)=>{
            response.status === 200?   toast.success("Deleted Successfully",{
                position:"bottom-right",
            })
            : toast.warning("Somthing went wrong",{
                position:"bottom-center",
            })

            response.status === 200 ? update(id):update('q');

        },
        (error)=>{
            toast.warning("Somthing went wrong",{
                position:"bottom-center",
            })
        }
    );
}







    return(
        <Card className="m-2">
            <CardBody className="text-center">
                <CardSubtitle className="font-weight-bold">{course.title}</CardSubtitle>
                <CardText>{course.description}</CardText>
                <Container >
                    
                    <Link to="/add-course"><Button color="secondary m-1" outline>Update</Button></Link>


                    <Button color="danger m-1"
                        onClick={ () => {
                            deleteCourse(course.id);
                        }}
                    >Delete</Button>
                    
                </Container>
            </CardBody>
        </Card>

    )
}




export default Course