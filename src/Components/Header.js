import React from "react";
import {Card,CardBody} from 'reactstrap';


function Header({ name, title}){

    return(
        <div>
            <Card className="my-2 bg-success">
                <CardBody>
                    <h1 className="text-center mt-2 mb-3 ">Welcome React Spring CRUD Application</h1>
                </CardBody>
            </Card>
            
        </div>
    )
}

export default Header;