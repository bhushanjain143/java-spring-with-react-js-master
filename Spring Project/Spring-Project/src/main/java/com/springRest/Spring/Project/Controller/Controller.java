package com.springRest.Spring.Project.Controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springRest.Spring.Project.Entity.Course;
import com.springRest.Spring.Project.Services.CourseService;

//REST :- Representional state transfer


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class Controller {
//for creating and initilize automatically object "service" we use annotation -> Autowired
	@Autowired
	private CourseService service;
//so spring will create CourseServiceImpl object which contain implementation method of CourseService
	
	
	@GetMapping("/home") 
	public String home() {
		return "Welcome";
	}
	
	
	//Get the Courses
	
	@GetMapping("/courses")//instead of this you can write also  @RequestMapping(path="/courses", method=Request.GET) but its lenghty
	public List<Course> getCourses(){
		
		
		return this.service.getCourses();//this working is knows as loose coupling
		//instead of creating derived class object we created base interface class object
	}
	
	
	//Get course using ID
	@GetMapping("/courses/{id}") 
	public Course getCourse(@PathVariable String id) {
		
		return this.service.getCourse(id);
		//return new Course(00,"not","not");
	}
	
	//Update course
	@PostMapping("/courses")
	public Course addCourse(@RequestBody Course course) {
		
		return this.service.addCourse(course);
	}
	
	
	//Adding course
	@PutMapping("/courses")
	public Course updateCourse(@RequestBody Course course) {
		
		return this.service.updateCourse(course);
	}
	
	
	//delete Course
	@DeleteMapping("/course/{id}")//trying to give server error is some bas occurs

	public ResponseEntity<HttpStatus> deleteCourse(@PathVariable String id) {
		  
		try {
			 this.service.deleteCourse(id);
			 return new ResponseEntity(HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
	}


}
 



















